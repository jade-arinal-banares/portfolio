const btnsend = document.querySelector("#btnsend");
const form = document.querySelector("#sendEmail");
const email = document.querySelector("#email");
const message = document.querySelector("#message");

const msg = document.querySelector("#msg")

btnsend.addEventListener("click", function(e){
   e.preventDefault();
   
   if(email.value.length === 0){
        msg.innerHTML = "<h4 class='error' >Email is required field</h4>"
        setTimeout(() => document.querySelector(".error").remove(), 3000);
    }else if (message.value.length === 0){
        msg.innerHTML = "<h4 class='error' >Message is required field</h4>"
        setTimeout(() => document.querySelector(".error").remove(), 3000);
    }else{
        form.submit(); 
    }
});


