$(window).scroll(function() {
    if ($(this).scrollTop()) {
        $('#toTop').fadeIn();
    } else {
        $('#toTop').fadeOut();
    }
});

$("#toTop").click(function () {
   $("html, body").animate({scrollTop: 0}, 1000);
});


//Modal Form
var lofiModal = document.getElementById("modal-lofi")
var lofiBtn = document.getElementById("modalBtn");

var descModal = document.getElementById("modal-desc");
var btn = document.getElementById("myBtn");


// Close
 var span1 = document.getElementsByClassName("closedesc")[0];
 var span = document.getElementsByClassName("closelofi")[0];
 // When the user clicks the button, open the modal 
 btn.onclick = function() {
    descModal.style.display = "block";
 }

 lofiBtn.onclick = function() {
    lofiModal.style.display = "block";
}
 
 // When the user clicks on <span> (x), close the modal
 span1.onclick = function() {
    descModal.style.display = "none";
 }
 span.onclick = function() {
    lofiModal.style.display = "none";
}
 
 // When the user clicks anywhere outside of the modal, close it
 window.onclick = function(event) {
   if (event.target == descModal) {
    descModal.style.display = "none";
   }
 }

 window.onclick = function(event) {
    if (event.target == lofiModal) {
    lofiModal.style.display = "none";
    }
}


function reveal() {
    let reveals = document.querySelectorAll(".reveal");
  
    for (var i = 0; i < reveals.length; i++) {
      let windowHeight = window.innerHeight;
      let elementTop = reveals[i].getBoundingClientRect().top;
      let elementVisible = 150;
  
      if (elementTop < windowHeight - elementVisible) {
        reveals[i].classList.add("active");
      } else {
        reveals[i].classList.remove("active");
      }
    }
  }
  
  window.addEventListener("scroll", reveal);


  function openNav() {
    document.getElementById("mySidenav").style.display = "block";
    document.body.style.backgroundColor = "rgba(0,0,0,0.7)";
}

function closeNav() {
    document.getElementById("mySidenav").style.display = "none";
    document.body.style.backgroundColor = "white";
}

